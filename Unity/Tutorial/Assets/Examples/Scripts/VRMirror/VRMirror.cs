﻿using UnityEngine;

public class VRMirror : MonoBehaviour
{
    [Header("Mirroring")]
    [Tooltip("This check box will activate the Mirroring to the left hand")]
    public bool mirrorLeft;

    [Tooltip("This check box will activate the Mirroring to the right hand")]
    public bool mirrorRight;

    [Header("Hands")]
    [Tooltip("Assign the Left hand component ('OVRCustomHandPrefab_L')")]
    //left hand gameObject in OVRCameraRig
    public GameObject leftHand;

    [Tooltip("Assign the Right hand component ('OVRCustomHandPrefab_R')")]
    //right hand gameObject in OVRCameraRig
    public GameObject rightHand;

    [Header("Hands Anchors")]
    [Tooltip("Assign the Left hand anchor component ('LeftHandAnchor')")]
    //left hand anchor from OVRCameraRig
    public GameObject leftHandAnchor;

    [Tooltip("Assign the Right hand anchor component ('RightHandAnchor)")]
    //right hand anchor from OVRCameraRig
    public GameObject rightHandAnchor;

    [Header("Player Transform")]
    [Tooltip("Assign the Camera Rig component")]
    public Transform playerTransform;

    // the curre position we are having while mirroring
    Vector3 currentPosition;

    // Generic boolean reference for mirroring
    public bool mirroring;

    /// <summary>
    /// This Function is the primary function that change the type of the hand that you want to mirror
    /// and need to be associated to the "MirrorFromTo()" function in the Update.
    /// </summary>
    public void EnableMirror()
    {
        if (mirrorLeft)
        {
            leftHand.SetActive(false);
            rightHand.GetComponent<OVRHand>().HandType = OVRHand.Hand.HandLeft;
        }
        else if (mirrorRight)
        {
            rightHand.SetActive(false);
            leftHand.GetComponent<OVRHand>().HandType = OVRHand.Hand.HandRight;
        }
    }

    /// <summary>
    /// This Function disable the mirroring putting the type of the hand on the starting settings
    /// </summary>
    public void DisableMirror()
    {
        leftHand.SetActive(true);
        rightHand.SetActive(true);
        leftHand.GetComponent<OVRHand>().HandType = OVRHand.Hand.HandLeft;
        rightHand.GetComponent<OVRHand>().HandType = OVRHand.Hand.HandRight;
        mirrorLeft = false;
        mirrorRight = false;
        mirroring = false;
    }

    /// <summary>
    /// This Function is important to be assigned together with the "DisableMirror()" for reset the
    /// transform of the hands in the correct anchor
    /// </summary>
    public void ResetMirror()
    {
        leftHand.transform.position = leftHandAnchor.transform.position;
        leftHand.transform.rotation = leftHandAnchor.transform.rotation;
        rightHand.transform.position = rightHandAnchor.transform.position;
        rightHand.transform.rotation = rightHandAnchor.transform.rotation;
    }

    // In the Update in base at the boolean that we set to true, we are going to Mirror the hand
    // in the specific Direction
    void Update()
    {
        Transform left = leftHand.GetComponent<Transform>();
        Transform right = rightHand.GetComponent<Transform>();

        if (mirrorLeft)
            MirrorFromTo(left, right);
        else if (mirrorRight)
            MirrorFromTo(right, left);
    }

    // Needed to set the current Direction of the hand 
    public void SetCurrentTransform()
    {
        currentPosition = playerTransform.right;
    }

    // This will mirror the hand from the source to the destination transform
    public void MirrorFromTo(Transform sourceTransform, Transform destTransform)
    {
        mirroring = true;

        // Determinate the position
        Vector3 playerToSourceHand = sourceTransform.position - playerTransform.position;
        Vector3 playerToDestHand = ReflectRelativeVector(playerToSourceHand);
        destTransform.position = playerTransform.position + playerToDestHand;
        // Determinate the rotation
        Vector3 forwardVec = ReflectRelativeVector(sourceTransform.forward);
        Vector3 upVec = ReflectRelativeVector(sourceTransform.up);
        destTransform.rotation = Quaternion.LookRotation(-forwardVec, -upVec);
    }

    Vector3 ReflectRelativeVector(Vector3 relativeVec)
    {
        // relativeVec
        //     Take the relative vector....
        // + Vector3.Dot(relativeVec, playerTransform.right)
        //     and for how far along the player's right direction it is 
        //     away from the player (may be negative),
        // * playerTransform.right
        //     move it that distance along the player's right...
        // * -2f
        //    negative two times (i.e., along the left direction 2x)

        return relativeVec
        + Vector3.Dot(relativeVec, currentPosition)
            * currentPosition
            * -2f;
    }
}