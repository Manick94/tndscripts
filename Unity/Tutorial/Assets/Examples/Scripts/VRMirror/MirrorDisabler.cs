using UnityEngine;

public class MirrorDisabler : MonoBehaviour
{
    // Reference to the VRMirror script
    public VRMirror vrMirror;

    // When we enter on a collider
    public void OnTriggerEnter(Collider collider)
    {
        Debug.Log($"I'm colliding with {collider.gameObject.name}");

        // in base of the tag of the collider gameObject and if we are mirroring
        if(collider.gameObject.tag == "Hands" && vrMirror.mirroring)
        {                          
            vrMirror.DisableMirror();
            vrMirror.ResetMirror();
        }
    }
}